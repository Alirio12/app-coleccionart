package com.adris.coleccionart.ui.authentication

import android.util.Log
import com.adris.coleccionart.common.preference.PreferenceModule
import com.adris.coleccionart.common.singleton.InstanceRetrofit

class RepositoryAuthentication private constructor(private val responseAuthentication: ResponseAuthentication) {

    companion object {
        fun instance(responseAuthentication: ResponseAuthentication) =
            RepositoryAuthentication(responseAuthentication)
    }

    suspend fun verifyDocument(type: String, document: String) {
        try {
            val data = InstanceRetrofit.instanceRetrofit!!.serviceApi.verifyDocument(type, document)
            if (data.code() == 200) {
                if (data.body() != null) {
                    if (data.body()!!.status == 200) {
                        Log.d("authenticationU", data.body().toString())
                        responseAuthentication.successVerifyDocument(data.body()!!)
                    } else {
                        responseAuthentication.errorVerifyDocument(data.body()!!.message)
                    }
                }
            } else {
                responseAuthentication.errorVerifyDocument("Error de conexión con el servidor")
            }

        } catch (e: Exception) {
            Log.d("authenticationE", e.toString())
            responseAuthentication.errorVerifyDocument("Error por tiempo de respuesta")
        }
    }

    suspend fun loadCities(city: String) {
        try {
            val data = InstanceRetrofit.instanceRetrofit!!.serviceApi.loadCitys(city)
            if (data.code() == 200) {
                if (data.body() != null) {
                    if (data.body()!!.status == 200) {
                        if (data.body()!!.cities.isNotEmpty()) {
                            Log.d("authenticationU", data.body().toString())
                            responseAuthentication.cities(data.body()!!.cities)
                        } else {
                            responseAuthentication.citiesEmpty("Ingrese una ciudad o municipio valido")
                        }
//                        responseAuthentication.successVerifyDocument(data.body()!!)
                    } else {
//                        responseAuthentication.errorVerifyDocument(data.body()!!.message)
                    }
                }
            } else {
                Log.d("authenticationE", "Error de conexión con el servidor")
                responseAuthentication.errorCities("Error de conexión con el servidor")
            }

        } catch (e: Exception) {
            Log.d("authenticationE", e.toString())
            responseAuthentication.errorCities("Error por tiempo de respuesta")
        }
    }


    suspend fun registerUser(
        first_name: String,
        last_name: String,
        email: String,
        username: String,
        document_type: String,
        document_number: String,
        address: String,
        phone: String,
        city_id: Long,
        password: String,
        device_token: String,
    ) {
        try {
            val data = InstanceRetrofit.instanceRetrofit!!.serviceApi.registerUser(
                first_name,
                last_name,
                email,
                username,
                document_type,
                document_number,
                address,
                phone,
                city_id,
                password,
                device_token,
            )
            when {
                data.code() == 201 -> {
                    if (data.body() != null) {
                        val dataUser = data.body()!!["data"].asJsonObject
                        PreferenceModule.getInstance().setDataRegister(
                            dataUser["username"].asString,
                            dataUser["first_name"].asString,
                            dataUser["last_name"].asString,
                            dataUser["email"].asString,
                            dataUser["document_number"].asString,
                            dataUser["address"].asString,
                            dataUser["phone"].asString,
                            dataUser["id"].asInt,
                            false
                        )
                        responseAuthentication.successRegister(true)
                        Log.d("authenticationUs", data.body().toString())
                    }
                }
                data.code() == 200 -> {
                    if (data.body()!!["status"].asInt == 422) {
                        when {
                            data.body()!!["data"].asJsonObject.has("phone") -> responseAuthentication.errorRegister(
                                "phone"
                            )
                            data.body()!!["data"].asJsonObject.has("username") -> responseAuthentication.errorRegister(
                                "username"
                            )
                            data.body()!!["data"].asJsonObject.has("document_number") -> responseAuthentication.errorRegister(
                                "document_number"
                            )
                            data.body()!!["data"].asJsonObject.has("email") -> responseAuthentication.errorRegister(
                                "email"
                            )
                        }
                    }
                }
                else -> {
                    Log.d("authenticationE", data.toString())
                    responseAuthentication.errorRegister("Error de conexión con el servidor")
                }
            }

        } catch (e: Exception) {
            Log.d("authenticationE", e.toString())
            responseAuthentication.errorRegister("Error por tiempo de respuesta")
        }
    }

    suspend fun verifyCode(email: String, code: String) {
        try {
            val data = InstanceRetrofit.instanceRetrofit!!.serviceApi.verifyCode(email, code)
            when {
                data.code() == 200 -> {
                    if (data.body() != null) {
                        if (data.body()!!["status"].asInt == 200) {
                            PreferenceModule.getInstance().setDataVerify(
                                data.body()!!["token"].asString,
                                data.body()!!["user"].asJsonObject["verified"].asBoolean
                            )
                            Log.d("authenticationVer", data.body().toString())
                            responseAuthentication.okCode(true)
                        }
                    }
                }
                data.code() == 404 -> {
                    responseAuthentication.errorCode("¡El código es incorrecto!")
                }

                else -> {
                    responseAuthentication.errorCode("Error de conexión con el servidor")
                }
            }

        } catch (e: Exception) {
            Log.d("authenticationE", e.toString())
            responseAuthentication.errorCode("Error por tiempo de respuesta")
        }
    }


    suspend fun sendNewCode(email: String) {
        try {
            val data = InstanceRetrofit.instanceRetrofit!!.serviceApi.sendNewCode(email)
            when {
                data.code() == 201 -> {
                    if (data.body() != null) {
                        responseAuthentication.newCode(true)
                    }
                }
                else -> {
                    responseAuthentication.errorNewCode("Error de conexión con el servidor")
                }
            }

        } catch (e: Exception) {
            Log.d("authenticationE", e.toString())
            responseAuthentication.errorNewCode("Error por tiempo de respuesta")
        }
    }

    suspend fun login(emailOrUserName: String, password: String) {
        try {
            val data =
                InstanceRetrofit.instanceRetrofit!!.serviceApi.login(emailOrUserName, password)
            when {
                data.code() == 200 -> {
                    if (data.body() != null) {
                        when (data.body()!!["status"].asInt) {
                            200 -> {
                                val user = data.body()!!["user"].asJsonObject
                                var verified = false
                                if (user["verified"].asInt == 1) {
                                    verified = true
                                }
                                Log.d("authenticationLogin", data.body().toString())
                                PreferenceModule.getInstance().setDataLogin(
                                    user["username"].asString,
                                    user["first_name"].asString,
                                    user["last_name"].asString,
                                    user["email"].asString,
                                    user["document_number"].asString,
                                    user["address"].asString,
                                    user["phone"].asString,
                                    user["id"].asInt,
                                    verified,
                                    data.body()!!["token"].asString
                                )
                                responseAuthentication.login(true)
                            }
                            401 -> {
                                responseAuthentication.errorLogin("Usuario o contraseña incorrectos")
                            }
                            else -> {
                                responseAuthentication.errorLogin("Error de conexión con el servidor")
                            }
                        }
                    }
                }
                else -> {
                    Log.d("authenticationLogin", data.body().toString())
                    responseAuthentication.errorLogin("Error de conexión con el servidor")
                }
            }

        } catch (e: Exception) {
            Log.d("authenticationE", e.toString())
            responseAuthentication.errorLogin("Error por tiempo de respuesta")
        }
    }

    suspend fun forgotPassword(emailOrUserName: String) {
        try {
            val data =
                InstanceRetrofit.instanceRetrofit!!.serviceApi.forgotPassword(emailOrUserName)
            when {
                data.code() == 200 -> {
                    if (data.body() != null) {
                        when (data.body()!!["status"].asString) {
                            "Success" -> {
                                Log.d("authenticationForg", data.body().toString())
                                responseAuthentication.forgotPassword(data.body()!!["data"].asJsonObject)
                            }
                            "Error" -> {
                                responseAuthentication.errorForgotPassword("Ingresa un email valido")
                                Log.d("authenticationForg", data.body().toString())
                            }
                            else -> {
                                responseAuthentication.errorForgotPassword("Error de conexión con el servidor")
                                Log.d("authenticationErForg", data.body().toString())
                            }
                        }

                    }
                }
                else -> {
                    Log.d("authenticationForg", data.body().toString())
                    responseAuthentication.errorForgotPassword("Error de conexión con el servidor")
//                    responseAuthentication.errorLogin("Error de conexión con el servidor")
                }
            }

        } catch (e: Exception) {
            Log.d("authenticationEFor", e.toString())
            responseAuthentication.errorForgotPassword("Error por tiempo de respuesta")
//            responseAuthentication.errorLogin("Error por tiempo de respuesta")
        }
    }

    suspend fun updatePassword(
        email: String,
        token: String,
        password: String,
        password_confirmation: String
    ) {
        try {
            val data =
                InstanceRetrofit.instanceRetrofit!!.serviceApi.updatePassword(
                    email,
                    token,
                    password,
                    password_confirmation
                )
            when {
                data.code() == 200 -> {
                    if (data.body() != null) {
                        when (data.body()!!["status"].asString) {
                            "Success" -> {
                                Log.d("authenticationRese", data.body().toString())
                                responseAuthentication.resetPassword(true)
                            }
                            "Error" -> {
                                responseAuthentication.errorResetPassword("Los datos no son correctos, vuelve a iniciar el proceso o intenta más tarde…")
                                Log.d("authenticationRes", data.body().toString())
                            }
                            else -> {
                                responseAuthentication.errorResetPassword("Error de conexión con el servidor")
                                Log.d("authenticationErRes", data.body().toString())
                            }
                        }

                    }
                }
                else -> {
                    Log.d("authenticationRes", data.body().toString())
                    responseAuthentication.errorResetPassword("Error de conexión con el servidor")
                }
            }

        } catch (e: Exception) {
            Log.d("authenticationERes", e.toString())
            responseAuthentication.errorResetPassword("Error por tiempo de respuesta")
        }
    }


}