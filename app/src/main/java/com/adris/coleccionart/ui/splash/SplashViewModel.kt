package com.adris.coleccionart.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class SplashViewModel : ViewModel(), ResponseSettings {

    private val repository = RepositorySettings.instance(this)

    val data: LiveData<Boolean> get() = _data
    val error: LiveData<String> get() = _error

    private var _data = MutableLiveData<Boolean>()
    private var _error = MutableLiveData<String>()

    fun loadSettings() {
        viewModelScope.launch {
            repository.loadSettings()
        }
    }

    override fun successData(success: Boolean) {
        _data.postValue(success)
    }

    override fun errorData(error: String) {
        _error.postValue(error)
    }
}