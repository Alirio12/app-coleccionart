package com.adris.coleccionart.ui.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.adris.coleccionart.R

class GalleryFragment : Fragment() {

    private lateinit var galleryViewModel: GalleryViewModel
    private val viewModel: GalleryViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toast.makeText(requireContext(), "gallery", Toast.LENGTH_LONG).show()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        viewModel =
//            ViewModelProvider(this).get(GalleryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)
        val textView: TextView = root.findViewById(R.id.text_gallery)
        viewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        /**en perfil validar si ha iniciado sesion, de no ser así, redireccionarlo al fragment de no login y apartir de ahí el usuario elige
         * que quiere hacer y lo enviariamos a un activity aparte para login o registro, de esta manera al devolverse sin loguearse
         * mostraría la pantalla de no login y no se quedaría en un bucle enviandolo a un activity diferente al no estar logueado.*/
        return root
    }
}