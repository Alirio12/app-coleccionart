package com.adris.coleccionart.ui.splash

import android.util.Log
import com.adris.coleccionart.common.singleton.InstanceRetrofit

class RepositorySettings(private val responseSettings: ResponseSettings) {

    companion object {
        fun instance(responseSettings: ResponseSettings) = RepositorySettings(responseSettings)
    }

    suspend fun loadSettings() {
        try {
            val data = InstanceRetrofit.instanceRetrofit!!.serviceApi.loadSettings()
            if (InstanceRetrofit.listTypeDocuments == null) {
                InstanceRetrofit.listTypeDocuments = ArrayList()
            }

            if (data.code() == 200) {
                if (data.body() != null) {
                    if (data.body()!!.typeDocuments.isEmpty()) {
                        responseSettings.errorData("No hay datos")
                    } else {
                        InstanceRetrofit.listTypeDocuments!!.addAll(data.body()!!.typeDocuments)
                        Log.d("errorRe", data.body().toString())
                        responseSettings.successData(true)
                    }
                } else {
                    responseSettings.errorData("No hay datos")
                }
            } else {
                responseSettings.errorData("Error en el código de respuesta")
            }
        } catch (e: Exception) {
            Log.d("errorRe", e.toString())
            responseSettings.errorData(e.message.toString())
        }
    }
}