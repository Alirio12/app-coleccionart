package com.adris.coleccionart.common.entities

data class TypeDocumentVO(
    val short_name: String,
    val name: String
) {
}