package com.adris.coleccionart.ui.authentication.changePassword

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import com.adris.coleccionart.R
import com.adris.coleccionart.databinding.ActivityForgotPasswordBinding
import com.adris.coleccionart.databinding.ItemViewLoadingBinding
import com.adris.coleccionart.ui.authentication.login.LoginViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class ForgotPasswordActivity : AppCompatActivity() {

    private lateinit var binding: ActivityForgotPasswordBinding
    private val viewModel: LoginViewModel by viewModels()
    private var waitingDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar2.setNavigationOnClickListener {
            onBackPressed()
        }

        binding.btnSend.setOnClickListener {
            binding.tiEmail.isErrorEnabled = false
            when {
                binding.etEmail.text.toString().trim().isEmpty() -> binding.tiEmail.error =
                    "Ingresa tu email"

                else -> {
                    viewModel.forgotPassword(binding.etEmail.text.toString().trim())
                    showLoading("Enviando email, por favor espera…")
                    it.hideKeyboard()
                }
            }
        }

        observe()
    }

    private fun observe() {
        viewModel.forgorPassword.observe(this, {
            waitingDialog!!.dismiss()
            val intent = Intent(this, ChangePasswordActivity::class.java).apply {
                putExtra("email", it["email"].asString)
                putExtra("token", it["token"].asString)
            }
            startActivity(intent)
            finish()
//            Toast.makeText(this, it.toString(), Toast.LENGTH_LONG).show()
        })

        viewModel.errorForgot.observe(this, {
            waitingDialog!!.dismiss()
            if (it == "Ingresa un email valido") {
                binding.tiEmail.error = it
            } else {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun showLoading(message: String) {
        val inflater = layoutInflater
        val view =
            inflater.inflate(R.layout.item_view_loading, findViewById(android.R.id.content), false)
        val binding: ItemViewLoadingBinding = ItemViewLoadingBinding.bind(view)
        binding.txtMessage.text = message
        val alerta = MaterialAlertDialogBuilder(this)
        alerta.setView(view)
        alerta.setCancelable(false)
        waitingDialog = alerta.create()
        waitingDialog!!.show()
    }
}