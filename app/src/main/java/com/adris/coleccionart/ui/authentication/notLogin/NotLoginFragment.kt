package com.adris.coleccionart.ui.authentication.notLogin

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.adris.coleccionart.R
import com.adris.coleccionart.common.preference.PreferenceModule
import com.adris.coleccionart.databinding.FragmentNotLoginBinding
import com.adris.coleccionart.ui.authentication.login.LoginActivity
import com.adris.coleccionart.ui.authentication.register.RegisterOneActivity
import com.google.android.material.bottomnavigation.BottomNavigationView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [NotLoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NotLoginFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentNotLoginBinding
    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var navControler: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        Toast.makeText(requireContext(), "created", Toast.LENGTH_LONG).show()
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNotLoginBinding.inflate(layoutInflater, container, false)
        bottomNavigationView =
            requireActivity().findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        navControler = findNavController()

        bottomNavigationView.isVisible = false


        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnCreate.setOnClickListener {
            val intent = Intent(requireContext(), RegisterOneActivity::class.java)
            startActivity(intent)
        }

        binding.btnLogin.setOnClickListener {
            val intent = Intent(requireContext(), LoginActivity::class.java)
            startActivity(intent)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NotLoginFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NotLoginFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        bottomNavigationView.isVisible = true
    }

    override fun onResume() {
        super.onResume()

        when {
            PreferenceModule.getInstance().existData() && !PreferenceModule.getInstance()
                .getVerified() -> {
                val navController = findNavController()
                navController.popBackStack()
            }
            PreferenceModule.getInstance().existData() && PreferenceModule.getInstance()
                .getVerified() -> {
                navControler.popBackStack()
            }
        }

    }
}