package com.adris.coleccionart.common.entities

data class CitiesVO(
    val id: Long,
    val name: String,
    val department_id: Long,
    val fullName: String
) {
}