package com.adris.coleccionart.ui.authentication.login

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import com.adris.coleccionart.R
import com.adris.coleccionart.databinding.ActivityLoginBinding
import com.adris.coleccionart.databinding.ItemViewLoadingBinding
import com.adris.coleccionart.ui.authentication.changePassword.ForgotPasswordActivity
import com.adris.coleccionart.ui.authentication.register.RegisterOneActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private val viewModel: LoginViewModel by viewModels()
    private var waitingDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar2.setNavigationOnClickListener {
            onBackPressed()
        }

        binding.btnLogin.setOnClickListener {
            binding.tiUser.isErrorEnabled = false
            binding.tiPassword.isErrorEnabled = false
            when {
                binding.etUser.text.toString().trim().isEmpty() -> binding.tiUser.error =
                    "Debe ingresar su usuario o email"
                binding.etPassword.text.toString().trim().isEmpty() -> binding.tiPassword.error =
                    "Debe ingresar su contraseña"

                else -> {
                    it.hideKeyboard()
                    showLoading("Verificando información, por favor espera…")
                    viewModel.loginUser(
                        binding.etUser.text.toString().trim(),
                        binding.etPassword.text.toString().trim()
                    )
                }
            }
        }

        binding.btnCreateAccount.setOnClickListener {
            val intent = Intent(this, RegisterOneActivity::class.java)
            startActivity(intent)
            finish()
        }

        binding.btnChangePassword.setOnClickListener {
            val intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }

        observe()
    }

    private fun observe() {
        viewModel.login.observe(this, {
            waitingDialog!!.dismiss()
            finish()
        })

        viewModel.errorLogin.observe(this, {
            waitingDialog!!.dismiss()
            if (it == "Usuario o contraseña incorrectos") {
                binding.tiUser.error = it
                binding.tiPassword.error = it
            }
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun showLoading(message: String) {
        val inflater = layoutInflater
        val view =
            inflater.inflate(R.layout.item_view_loading, findViewById(android.R.id.content), false)
        val binding: ItemViewLoadingBinding = ItemViewLoadingBinding.bind(view)
        binding.txtMessage.text = message
        val alerta = MaterialAlertDialogBuilder(this)
        alerta.setView(view)
        alerta.setCancelable(true)
        waitingDialog = alerta.create()
        waitingDialog!!.show()
    }
}