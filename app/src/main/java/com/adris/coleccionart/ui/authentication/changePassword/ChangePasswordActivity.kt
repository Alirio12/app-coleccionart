package com.adris.coleccionart.ui.authentication.changePassword

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import com.adris.coleccionart.R
import com.adris.coleccionart.databinding.ActivityChangePasswordBinding
import com.adris.coleccionart.databinding.ItemViewLoadingBinding
import com.adris.coleccionart.ui.authentication.login.LoginViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class ChangePasswordActivity : AppCompatActivity() {

    private lateinit var binding: ActivityChangePasswordBinding
    private val viewModel: LoginViewModel by viewModels()
    private var email: String? = null
    private var token: String? = null
    private var waitingDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        email = intent.getStringExtra("email")
        token = intent.getStringExtra("token")

        binding.toolbar2.setNavigationOnClickListener {
            showAlert()
        }

        binding.btnSend.setOnClickListener {
            binding.tiPassword.isErrorEnabled = false
            binding.tiPasswordConfirm.isErrorEnabled = false
            when {
                binding.etPassword.text.toString().trim().isEmpty() -> binding.tiPassword.error =
                    "Ingrese una contraseña"
                binding.etPasswordConfirm.text.toString().trim()
                    .isEmpty() || binding.etPassword.text.toString()
                    .trim() != binding.etPasswordConfirm.text.toString()
                    .trim() -> binding.tiPasswordConfirm.error =
                    "Las contraseñas no coinciden"
                binding.etPassword.text!!.length < 8 -> binding.tiPassword.error =
                    "La contraseña debe tener al menos 8 caracteres"
                else -> {
                    viewModel.updatePasswordUser(
                        email.toString(),
                        token.toString(),
                        binding.etPassword.text.toString().trim(),
                        binding.etPasswordConfirm.text.toString().trim()
                    )
                    it.hideKeyboard()
                    showLoading("Actualizando la contraseña, ppor favor espera...")
                }
            }
        }
//        Toast.makeText(this, intent.getStringExtra("email"), Toast.LENGTH_LONG).show()
//        Toast.makeText(this, intent.getStringExtra("token"), Toast.LENGTH_LONG).show()
        observe()
    }

    private fun observe() {
        viewModel.updatePassword.observe(this, {
            waitingDialog!!.dismiss()
            finish()
//            Toast.makeText(this, "Contraseña actualizada", Toast.LENGTH_LONG).show()
        })

        viewModel.errorUpdatePassword.observe(this, {
            waitingDialog!!.dismiss()
            binding.tiPassword.error = it
        })
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun showLoading(message: String) {
        val inflater = layoutInflater
        val view =
            inflater.inflate(R.layout.item_view_loading, findViewById(android.R.id.content), false)
        val binding: ItemViewLoadingBinding = ItemViewLoadingBinding.bind(view)
        binding.txtMessage.text = message
        val alerta = MaterialAlertDialogBuilder(this)
        alerta.setView(view)
        alerta.setCancelable(false)
        waitingDialog = alerta.create()
        waitingDialog!!.show()
    }

    private fun showAlert() {
        MaterialAlertDialogBuilder(this)
            .setTitle("¿Cancelar proceso?")
            .setMessage("Al salir se cancelará el proceso, ¿desea salir?")
            .setPositiveButton("Aceptar") { dialog, which ->
                finish()
            }
            .setNegativeButton("Cancelar") { dialog, which ->
                dialog.dismiss()
            }
            .setCancelable(false)
            .create().show()
    }

    override fun onBackPressed() {
        showAlert()
//        super.onBackPressed()
    }
}