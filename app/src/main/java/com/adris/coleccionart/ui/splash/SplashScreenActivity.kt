package com.adris.coleccionart.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import com.adris.coleccionart.MainActivity
import com.adris.coleccionart.R
import com.adris.coleccionart.common.preference.PreferenceModule
import com.adris.coleccionart.common.singleton.InstanceRetrofit
import com.google.firebase.iid.FirebaseInstanceId

class SplashScreenActivity : AppCompatActivity() {

    private val viewModel: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

//        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
//            Log.d("tokenFirebaseSplash", it.token)
//        }

        if (InstanceRetrofit.instanceRetrofit == null) {
            InstanceRetrofit.getInstanceRetrofit(this)
        }


        PreferenceModule.getInstance(this)

        viewModel.loadSettings()

        viewModel.data.observe(this, {
//            Toast.makeText(this, "siii", Toast.LENGTH_LONG).show()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        })

        viewModel.error.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            viewModel.loadSettings()
        })
    }
}