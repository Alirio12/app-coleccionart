package com.adris.coleccionart.common.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import com.adris.coleccionart.MainActivity
import com.adris.coleccionart.R
import com.adris.coleccionart.common.preference.PreferenceModule
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FcmMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d("tokenFirebaseNotifi", remoteMessage.notification.toString())
        if (remoteMessage.notification != null) {
            sendNotification(remoteMessage)
        }
    }

    private fun sendNotification(remoteMessage: RemoteMessage) {
//        val des = remoteMessage.data[FcmMessagingService.DESCUENTO].toFloat()
        val intent = Intent(this, MainActivity::class.java)
//        intent.putExtra(FcmMessagingService.DESCUENTO, des)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder: Notification.Builder = Notification.Builder(this)
            .setSmallIcon(R.drawable.ic_account)
            .setContentTitle(remoteMessage.notification!!.title)
            .setContentText(remoteMessage.notification!!.body)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            notificationBuilder.setColor(
//                if (des > .4) ContextCompat.getColor(
//                    applicationContext,
//                    R.color.colorPrimary
//                ) else ContextCompat.getColor(
//                    applicationContext, R.color.colorAccent
//                )
//            )
//        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = getString(R.string.normal_channel_id)
            val channelName = getString(R.string.normal_channel_name)
            val notificationChannel =
                NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(100, 200, 200, 50)
            notificationManager.createNotificationChannel(notificationChannel)
            notificationBuilder.setChannelId(channelId)
        }


        notificationManager.notify("", 0, notificationBuilder.build())
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        PreferenceModule.getInstance(this)

        PreferenceModule.getInstance().setTokenFirebase(token)
        Log.d("tokenFirebase", token)
    }
}