package com.adris.coleccionart.common.dataApi

import com.adris.coleccionart.common.entities.TypeDocumentVO

data class DataSettings(
    val typeDocuments: ArrayList<TypeDocumentVO>
) {
}