package com.adris.coleccionart.ui.authentication.register

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModel
import com.adris.coleccionart.MainActivity
import com.adris.coleccionart.R
import com.adris.coleccionart.common.preference.PreferenceModule
import com.adris.coleccionart.common.singleton.InstanceRetrofit
import com.adris.coleccionart.databinding.ActivityRegisterOneBinding
import com.adris.coleccionart.databinding.ItemViewLoadingBinding
import com.adris.coleccionart.ui.authentication.login.LoginActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class RegisterOneActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterOneBinding
    private val viewModel: RegisterViewModel by viewModels()
    private lateinit var adapterSelect: ArrayAdapter<String>
    private var waitingDialog: AlertDialog? = null
    private var document: String = ""
    private var typeDocument: String = ""
    private var names: String = ""
    private var lastNames: String = ""
    private var errorUserName = ""
    private var errorDocument = ""
    private var email: String? = null
    private var city: String? = null
    private var idCity: Long? = null
    private var direction: String? = null
    private var phone: String? = null
    private var password: String? = null
//    private var errorEmail: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterOneBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Log.d("tokenFirebaseRegis", PreferenceModule.getInstance().getTokenFirebase().toString())

        if (intent.getStringExtra("typeDocument") != null && intent.getStringExtra("document") != null) {
            typeDocument = intent.getStringExtra("typeDocument")!!
            document = intent.getStringExtra("document")!!
            names = intent.getStringExtra("names")!!
            lastNames = intent.getStringExtra("lastNames")!!
            binding.filledType.setText(typeDocument)
            binding.etDocument.setText(document)
            binding.etNames.setText(names)
            binding.etLastnames.setText(lastNames)
            binding.etUser.setText(intent.getStringExtra("userName"))
        }

        if (intent.getStringExtra("errorUserName") != null) {
            errorUserName = intent.getStringExtra("errorUserName")!!
            email = intent.getStringExtra("email")!!
            city = intent.getStringExtra("city")!!
            idCity = intent.getLongExtra("idCity", -1)
            direction = intent.getStringExtra("direction")!!
            phone = intent.getStringExtra("phone")!!
            password = intent.getStringExtra("password")!!
//            errorEmail = intent.getIntExtra("errorEmail", 0)
            binding.tiUser.error = errorUserName
        }

        if (intent.getStringExtra("errorDocument") != null) {
            errorDocument = intent.getStringExtra("errorDocument")!!
            email = intent.getStringExtra("email")!!
            city = intent.getStringExtra("city")!!
            idCity = intent.getLongExtra("idCity", -1)
            direction = intent.getStringExtra("direction")!!
            phone = intent.getStringExtra("phone")!!
            password = intent.getStringExtra("password")!!
//            errorEmail = intent.getIntExtra("errorEmail", 0)
            binding.tiDocument.error = errorDocument
            binding.etNames.setText("")
            binding.etLastnames.setText("")
        }


        val dataAdapter = ArrayList<String>()
        for (type in InstanceRetrofit.listTypeDocuments!!) {
            dataAdapter.add(type.short_name)
        }

        adapterSelect = ArrayAdapter(
            this,
            R.layout.item_view_spinner,
            dataAdapter
        )

        binding.filledType.setAdapter(adapterSelect)

        binding.filledType.setOnItemClickListener { parent, view, position, id ->
            binding.selectTypeDoc.isErrorEnabled = false
            for (type in InstanceRetrofit.listTypeDocuments!!) {
                if (type.short_name == parent.adapter.getItem(position).toString()) {
                    typeDocument = type.short_name
                    Toast.makeText(this, type.name, Toast.LENGTH_LONG).show()
                }
            }
        }


        binding.btnSearchDocument.setOnClickListener {
            binding.selectTypeDoc.isErrorEnabled = false
            binding.tiDocument.isErrorEnabled = false
            when {
                binding.filledType.text.toString().trim().isEmpty() -> binding.selectTypeDoc.error =
                    " "
                binding.etDocument.text.toString().trim().isEmpty() -> binding.tiDocument.error =
                    "Ingrese el documento"
                else -> {
                    it.hideKeyboard()
                    document = binding.etDocument.text.toString().trim()
                    viewModel.verifyDocument(
                        binding.filledType.text.toString().trim(),
                        binding.etDocument.text.toString().trim()
                    )
                    showLoading("Verificando información, por favor espera…")
                }
            }
        }

        binding.btnNext.setOnClickListener {
            when {
                binding.filledType.text.toString().trim().isEmpty() -> binding.selectTypeDoc.error =
                    " "
                binding.etDocument.text.toString().trim().isEmpty() -> binding.tiDocument.error =
                    "Ingrese el documento"

                binding.etNames.text.toString().trim().isEmpty() -> binding.tiNames.error =
                    "Verifique su información"
                binding.etLastnames.text.toString().trim().isEmpty() -> binding.tiLastnames.error =
                    "Verifique su información"

                document != binding.etDocument.text.toString().trim() -> binding.tiDocument.error =
                    "Ingrese el documento correcto"

                binding.etUser.text.toString().trim().isEmpty() -> binding.tiUser.error =
                    "Ingrese un usuario"

                else -> {
                    val intent = Intent(this, RegisterTwoActivity::class.java).apply {
                        putExtra("typeDocument", typeDocument)
                        putExtra("document", document)
                        putExtra("names", names)
                        putExtra("lastNames", lastNames)
                        putExtra("userName", binding.etUser.text.toString().trim())
                        putExtra("email", email)
                        putExtra("city", city)
                        putExtra("idCity", idCity)
                        putExtra("direction", direction)
                        putExtra("phone", phone)
                        putExtra("password", password)
//                        putExtra("errorEmail", errorEmail)
                    }
                    startActivity(intent)
                    finish()
                }
            }
        }

        binding.btnLogin.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        observe()
    }

    private fun observe() {
        viewModel.dataDocument.observe(this, {
//            Toast.makeText(this, it.firstName, Toast.LENGTH_LONG).show()
            names = it.firstName
            lastNames = it.lastName
            waitingDialog!!.dismiss()
            binding.etNames.setText(it.firstName)
            binding.etLastnames.setText(it.lastName)
        })

        viewModel.errorDocument.observe(this, {
//            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            binding.tiDocument.error = it
            waitingDialog!!.dismiss()
        })
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun showLoading(message: String) {
        val inflater = layoutInflater
        val view =
            inflater.inflate(R.layout.item_view_loading, findViewById(android.R.id.content), false)
        val binding: ItemViewLoadingBinding = ItemViewLoadingBinding.bind(view)
        binding.txtMessage.text = message
        val alerta = MaterialAlertDialogBuilder(this)
        alerta.setView(view)
        alerta.setCancelable(true)
//        alerta.setNegativeButton("Cancelar") { dialog, which ->
//
//        }
        waitingDialog = alerta.create()
        waitingDialog!!.show()
    }
}