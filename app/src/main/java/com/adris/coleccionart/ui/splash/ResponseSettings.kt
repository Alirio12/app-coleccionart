package com.adris.coleccionart.ui.splash

interface ResponseSettings {
    fun successData(success: Boolean)

    fun errorData(error: String)
}