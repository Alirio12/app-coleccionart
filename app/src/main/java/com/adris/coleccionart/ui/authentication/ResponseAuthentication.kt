package com.adris.coleccionart.ui.authentication

import com.adris.coleccionart.common.dataApi.DataDocument
import com.adris.coleccionart.common.entities.CitiesVO
import com.google.gson.JsonObject

interface ResponseAuthentication {

    fun successVerifyDocument(data: DataDocument)

    fun cities(cities: ArrayList<CitiesVO>)

    fun citiesEmpty(error: String)

    fun errorCities(error: String)

    fun errorVerifyDocument(error: String)

    fun successRegister(success: Boolean)

    fun okCode(success: Boolean)

    fun newCode(success: Boolean)

    fun errorRegister(error: String)

    fun errorCode(error: String)

    fun errorNewCode(error: String)

    fun login(success: Boolean)

    fun errorLogin(error: String)

    fun forgotPassword(data: JsonObject)

    fun errorForgotPassword(error: String)

    fun resetPassword(success: Boolean)

    fun errorResetPassword(error: String)
}
