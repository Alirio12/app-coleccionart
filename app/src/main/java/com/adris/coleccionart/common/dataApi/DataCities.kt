package com.adris.coleccionart.common.dataApi

import com.adris.coleccionart.common.entities.CitiesVO

data class DataCities(
    val status: Int,
    val cities: ArrayList<CitiesVO>
) {
}