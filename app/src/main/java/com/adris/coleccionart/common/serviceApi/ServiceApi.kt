package com.adris.coleccionart.common.serviceApi

import com.adris.coleccionart.common.dataApi.DataCities
import com.adris.coleccionart.common.dataApi.DataDocument
import com.adris.coleccionart.common.dataApi.DataSettings
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.*

interface ServiceApi {

    @GET("parameters/settings")
    suspend fun loadSettings(): Response<DataSettings>

    @FormUrlEncoded
    @POST("auth/getnames")
    suspend fun verifyDocument(
        @Field("document_type") type: String,
        @Field("document_number") document: String
    ): Response<DataDocument>

    @GET("parameters/searchCity")
    suspend fun loadCitys(@Query("city") city: String): Response<DataCities>

    @FormUrlEncoded
    @POST("auth/signup/app")
    suspend fun registerUser(
        @Field("first_name") first_name: String,
        @Field("last_name") last_name: String,
        @Field("email") email: String,
        @Field("username") username: String,
        @Field("document_type") document_type: String,
        @Field("document_number") document_number: String,
        @Field("address") address: String,
        @Field("phone") phone: String,
        @Field("city_id") city_id: Long,
        @Field("password") password: String,
        @Field("device_token") device_token: String
    ): Response<JsonObject>

    @FormUrlEncoded
    @POST("auth/verifyCodeRegister")
    suspend fun verifyCode(
        @Field("email") email: String,
        @Field("code") code: String
    ): Response<JsonObject>

    @FormUrlEncoded
    @POST("auth/sendNewCode")
    suspend fun sendNewCode(
        @Field("email") email: String
    ): Response<JsonObject>

    @FormUrlEncoded
    @POST("auth/login")
    suspend fun login(
        @Field("email_or_username") emailOrUserName: String,
        @Field("password") password: String
    ): Response<JsonObject>

    @FormUrlEncoded
    @POST("auth/forgotPassword")
    suspend fun forgotPassword(
        @Field("email_or_username") emailOrUserName: String
    ): Response<JsonObject>

    @FormUrlEncoded
    @POST("auth/resetPassword")
    suspend fun updatePassword(
        @Field("email") email: String,
        @Field("token") token: String,
        @Field("password") password: String,
        @Field("password_confirmation") password_confirmation: String,
    ): Response<JsonObject>
}