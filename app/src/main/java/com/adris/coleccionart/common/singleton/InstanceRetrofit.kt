package com.adris.coleccionart.common.singleton

import android.content.Context
import android.net.Uri
import android.view.View
import com.adris.coleccionart.R
import com.adris.coleccionart.common.entities.TypeDocumentVO
import com.adris.coleccionart.common.preference.PreferenceModule
import com.adris.coleccionart.common.serviceApi.ServiceApi
import com.google.android.material.snackbar.Snackbar
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class InstanceRetrofit private constructor(context: Context) {

    @JvmField
    var serviceApi: ServiceApi
    private var retrofit: Retrofit


    companion object {
        @get:Synchronized
        var instanceRetrofit: InstanceRetrofit? = null
            private set

        @JvmField
        var listTypeDocuments: ArrayList<TypeDocumentVO>? = null


        @Synchronized
        fun getInstanceRetrofit(context: Context) {
            if (instanceRetrofit == null) {
                instanceRetrofit = InstanceRetrofit(context)
            }
        }
    }

    init {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor { chain: Interceptor.Chain ->
                val newRequest = chain.request().newBuilder()
                    .addHeader(
                        "Authorization",
                        "Bearer " + PreferenceModule.getInstance().getToken()
                    )
                    .build()
                chain.proceed(newRequest)
            }
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()
        retrofit = Retrofit.Builder()
            .baseUrl(context.getString(R.string.ip_api))
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build()
        serviceApi = retrofit.create(ServiceApi::class.java)
    }
}