package com.adris.coleccionart.common.dataApi

data class DataDocument(
    val status: Int,
    val firstName: String,
    val lastName: String,
    val message: String
) {
}