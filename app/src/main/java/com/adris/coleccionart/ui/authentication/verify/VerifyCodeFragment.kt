package com.adris.coleccionart.ui.authentication.verify

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.adris.coleccionart.R
import com.adris.coleccionart.common.preference.PreferenceModule
import com.adris.coleccionart.databinding.FragmentVerifyCodeBinding
import com.adris.coleccionart.databinding.ItemViewLoadingBinding
import com.adris.coleccionart.ui.authentication.register.RegisterViewModel
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.lang.StringBuilder

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [VerifyCodeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class VerifyCodeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var binding: FragmentVerifyCodeBinding
    private val viewModel: RegisterViewModel by activityViewModels()
    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var toolbar: MaterialToolbar
    private lateinit var navController: NavController
    private var waitingDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentVerifyCodeBinding.inflate(layoutInflater, container, false)

        bottomNavigationView =
            requireActivity().findViewById<BottomNavigationView>(R.id.bottomNavigationView)

        toolbar = requireActivity().findViewById(R.id.toolbar)

        toolbar.isVisible = false

        bottomNavigationView.isVisible = false

        // Inflate the layout for this fragment
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = findNavController()

        val phone = PreferenceModule.getInstance().getPhone()!!.split("").toTypedArray()

        val result = StringBuilder()
        result.append(phone[0])
        result.append(phone[1])
        result.append(phone[2])
        result.append(phone[3])
        result.append("*")
        result.append("*")
        result.append("*")
        result.append("*")
        result.append("*")
        result.append(phone[phone.lastIndex - 2])
        result.append(phone[phone.lastIndex - 1])
//        var phoneFinish = ""

        val messageFinish =
            "Ingresa el código de verificación que fue enviado a tu número de teléfono $result (este código tiene una duración de 5 minutos)"
        binding.txtMessage.text = messageFinish

        binding.toolbar2.setNavigationOnClickListener {
            navController.popBackStack()
        }

        binding.btnVerify.setOnClickListener {
            when {
                binding.etCode.text.toString().trim().isEmpty() -> binding.tiCode.error =
                    "Debe ingresar el código"

                else -> {
                    binding.tiCode.isErrorEnabled = false
                    viewModel.verifyCode(
                        PreferenceModule.getInstance().getEmail()!!,
                        binding.etCode.text.toString().trim()
                    )
                    showLoading("Verificando código, por favor espera…")
                    it.hideKeyboard()
                }
            }
        }

        binding.btnSendCode.setOnClickListener {
            showLoading("Reenviando código, por favor espera…")
            viewModel.sendNewCode(PreferenceModule.getInstance().getEmail().toString())
        }

        observe()
    }

    private fun observe() {
        viewModel.code.observe(viewLifecycleOwner, {
            waitingDialog!!.dismiss()
            navController.popBackStack()
        })

        viewModel.newCode.observe(viewLifecycleOwner, {
            waitingDialog!!.dismiss()
            Toast.makeText(requireContext(), "Código reenviado", Toast.LENGTH_LONG).show()
        })

        viewModel.errorCode.observe(viewLifecycleOwner, {
            waitingDialog!!.dismiss()
            binding.tiCode.error = it
        })

        viewModel.errorNewCode.observe(viewLifecycleOwner, {
            waitingDialog!!.dismiss()
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment VerifyCodeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            VerifyCodeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun showLoading(message: String) {
        val inflater = layoutInflater
        val view =
            inflater.inflate(
                R.layout.item_view_loading,
                requireActivity().findViewById(android.R.id.content),
                false
            )
        val binding: ItemViewLoadingBinding = ItemViewLoadingBinding.bind(view)
        binding.txtMessage.text = message
        val alerta = MaterialAlertDialogBuilder(requireContext())
        alerta.setView(view)
        alerta.setCancelable(true)
//        alerta.setNegativeButton("Cancelar") { dialog, which ->
//
//        }
        waitingDialog = alerta.create()
        waitingDialog!!.show()
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        bottomNavigationView.isVisible = true
        toolbar.isVisible = true
    }
}