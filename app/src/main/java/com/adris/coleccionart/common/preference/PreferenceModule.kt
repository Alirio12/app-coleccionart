package com.adris.coleccionart.common.preference

import android.content.Context
import android.content.SharedPreferences

class PreferenceModule {

    companion object {
        @Volatile
        private var INSTANCE: PreferenceModule? = null

        //        private lateinit var context: Context
        private var preferences: SharedPreferences? = null

        @Synchronized
        fun getInstance(context: Context) {
            if (INSTANCE == null) {
                preferences = context.getSharedPreferences("dataUser", Context.MODE_PRIVATE)
                INSTANCE = PreferenceModule()
            }
        }

        @Synchronized
        fun getInstance(): PreferenceModule {
            return INSTANCE!!
        }
    }

    fun setDataRegister(
        userName: String,
        name: String,
        lastName: String,
        email: String,
        document: String,
        address: String,
        phone: String,
        idUser: Int,
        verified: Boolean,
    ) {
        val editor = preferences!!.edit()
        editor.putString("userName", userName)
        editor.putString("names", name)
        editor.putString("lastNames", lastName)
        editor.putString("email", email)
        editor.putString("document", document)
        editor.putString("address", address)
        editor.putString("phone", phone)
        editor.putInt("idUser", idUser)
        editor.putBoolean("verified", verified)
        editor.apply()
    }


    fun setDataVerify(token: String, verified: Boolean) {
        val editor = preferences!!.edit()
        editor.putString("api_token", token)
        editor.putBoolean("verified", verified)
        editor.apply()
    }

    fun setDataLogin(
        userName: String,
        name: String,
        lastName: String,
        email: String,
        document: String,
        address: String,
        phone: String,
        idUser: Int,
        verified: Boolean,
        token: String
    ) {
        val editor = preferences!!.edit()
        editor.putString("userName", userName)
        editor.putString("names", name)
        editor.putString("lastNames", lastName)
        editor.putString("email", email)
        editor.putString("document", document)
        editor.putString("address", address)
        editor.putString("phone", phone)
        editor.putInt("idUser", idUser)
        editor.putBoolean("verified", verified)
        editor.putString("api_token", token)
        editor.apply()
    }

    fun setPlan(plan: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean("plan", plan)
        editor.apply()
    }

    fun setTokenFirebase(token: String) {
        val editor = preferences!!.edit()
        editor.putString("firebaseToken", token)
        editor.apply()
    }

    fun getTokenFirebase(): String? {
        return preferences!!.getString("firebaseToken", "")
    }

    fun getToken(): String? {
        return preferences!!.getString("api_token", "")
    }


    fun getId(): Int {
        return preferences!!.getInt("id_cliente", -1)
    }

    fun getName(): String {
        return "${preferences!!.getString("names", "")} ${preferences!!.getString("lastNames", "")}"
    }

    fun getVerified(): Boolean {
        return preferences!!.getBoolean("verified", false)
    }

    fun getEmail(): String? {
        return preferences!!.getString("email", "")
    }

    fun getPhone(): String? {
        return preferences!!.getString("phone", "")
    }

    fun deleteInfo() {
        preferences!!.edit().clear().apply()
    }

    fun existData(): Boolean {
        return preferences!!.getInt("idUser", -1) != -1 && preferences!!.getString(
            "phone",
            ""
        ) != ""
    }

}