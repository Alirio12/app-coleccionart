package com.adris.coleccionart.ui.authentication.register

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import com.adris.coleccionart.R
import com.adris.coleccionart.common.entities.CitiesVO
import com.adris.coleccionart.common.preference.PreferenceModule
import com.adris.coleccionart.databinding.ActivityRegisterTwoBinding
import com.adris.coleccionart.databinding.ItemViewLoadingBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class RegisterTwoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterTwoBinding
    private val viewModel: RegisterViewModel by viewModels()
    private var waitingDialog: AlertDialog? = null
    private var document: String? = null
    private var typeDocument: String? = null
    private var names: String? = null
    private var lastNames: String? = null
    private var userName: String? = null
    private var idCity: Long? = null
    private lateinit var adapterCities: ArrayAdapter<String>
    private var loadCity = true
    private lateinit var listCities: ArrayList<CitiesVO>
    private val dataAdapter = ArrayList<String>()

    //    private var charactersPassword: Int = 0
//    private var errorEmail = 0
    private var errorUserName: String? = null
    private var errorDocument: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterTwoBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(binding.root)
        listCities = ArrayList()

        if (intent.getStringExtra("typeDocument") != null && intent.getStringExtra("document") != null) {
            typeDocument = intent.getStringExtra("typeDocument")!!
            document = intent.getStringExtra("document")!!
            names = intent.getStringExtra("names")!!
            lastNames = intent.getStringExtra("lastNames")!!
            userName = intent.getStringExtra("userName")!!
        }

        if (intent.getLongExtra("idCity", -1)
                .toInt() != -1 && intent.getStringExtra("email") != null
        ) {
            idCity = intent.getLongExtra("idCity", -1)
            binding.filledCity.setText(intent.getStringExtra("city"))
            binding.etEmail.setText(intent.getStringExtra("email"))
            binding.etDirection.setText(intent.getStringExtra("direction"))
            binding.etPhone.setText(intent.getStringExtra("phone"))
            binding.etPassword.setText(intent.getStringExtra("password"))
//            errorEmail = intent.getIntExtra("errorEmail", 0)
        }


        binding.toolbar.setNavigationOnClickListener {
            onBack()
        }



        binding.filledCity.setOnItemClickListener { parent, view, position, id ->
            binding.selectCity.isErrorEnabled = false
            for (city in listCities) {
                if (city.fullName == parent.adapter.getItem(position).toString()) {
                    idCity = city.id
//                    Toast.makeText(this, idCity.toString(), Toast.LENGTH_LONG).show()
                }
            }
        }


        binding.filledCity.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                Toast.makeText(applicationContext, count.toString(), Toast.LENGTH_LONG).show()
                when {
                    count == 5 && loadCity -> {
                        binding.selectCity.isErrorEnabled = false
                        viewModel.loadCities(s.toString().trim())
                        showLoading("Buscando coincidencias, por favor espere…")
                        view.hideKeyboard()
                    }
                    count == 7 && loadCity -> {
                        binding.selectCity.isErrorEnabled = false
                        viewModel.loadCities(s.toString().trim())
                        showLoading("Buscando coincidencias, por favor espere…")
                        view.hideKeyboard()
                    }
                    count < 2 -> loadCity = true
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })


//        binding.etEmail.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                val letter = s.toString().trim().substringAfterLast("")
//                if (letter == "@" || letter == ".") {
//                    errorEmail += 1
//                }
//            }
//
//            override fun afterTextChanged(s: Editable?) {}
//        })

        binding.btnCreated.setOnClickListener {
            binding.selectCity.isErrorEnabled = false
            binding.tiEmail.isErrorEnabled = false
            binding.tiDirection.isErrorEnabled = false
            binding.tiPhone.isErrorEnabled = false
            binding.tiPassword.isErrorEnabled = false
            binding.tiConfirmPassword.isErrorEnabled = false
            when {
                binding.filledCity.text.toString().trim()
                    .isEmpty() || idCity == null -> binding.selectCity.error =
                    "Ingresa tu ciudad de residencia"
                binding.etEmail.text.toString().trim().isEmpty() -> binding.tiEmail.error =
                    "Ingresa tu email"
                binding.etDirection.text.toString().trim().isEmpty() -> binding.tiDirection.error =
                    "Ingresa una dirección"
                binding.etPhone.text.toString().trim().isEmpty() -> binding.tiPhone.error =
                    "Ingresa tu teléfono"
                binding.etPassword.text.toString().trim().isEmpty() -> binding.tiPassword.error =
                    "Ingresa una contraseña"
                binding.etConfirmPassword.text.toString().trim()
                    .isEmpty() || binding.etConfirmPassword.text.toString()
                    .trim() != binding.etPassword.text.toString()
                    .trim() -> binding.tiConfirmPassword.error =
                    "Las contraseñas no coinciden"
                binding.etPassword.text!!.length < 8 -> binding.tiPassword.error =
                    "La contraseña debe tener al menos 8 caracteres"
//                errorEmail < 2 -> binding.tiEmail.error = "¡Ingrese un email válido!"
                else -> {
                    viewModel.registerUser(
                        names!!,
                        lastNames!!,
                        binding.etEmail.text.toString().trim(),
                        userName!!,
                        typeDocument!!,
                        document!!,
                        binding.etDirection.text.toString().trim(),
                        binding.etPhone.text.toString().trim(),
                        idCity!!,
                        binding.etPassword.text.toString().trim(),
                        PreferenceModule.getInstance().getTokenFirebase().toString()
                    )
                    showLoading("Registrando información, por favor espere…")
                }
            }
        }

        observe()
    }

    private fun observe() {
        viewModel.cities.observe(this, {
            dataAdapter.clear()
            listCities.addAll(it)
            loadCity = false

            for (city in it) {
                dataAdapter.add(city.fullName)
            }

            adapterCities = ArrayAdapter(this, R.layout.item_view_spinner, dataAdapter)

            Log.d("authenticationU", dataAdapter.toString())
            binding.filledCity.setAdapter(adapterCities)
            waitingDialog!!.dismiss()
        })

        viewModel.register.observe(this, {
            waitingDialog!!.dismiss()
            finish()
//            Toast.makeText(this, "Registrado", Toast.LENGTH_LONG).show()
        })

        viewModel.citiesEmpty.observe(this, {
            waitingDialog!!.dismiss()
            binding.selectCity.error = it
        })

        viewModel.errorCities.observe(this, {
//            loadCity = true
            waitingDialog!!.dismiss()
//            viewModel.loadCities(binding.filledCity.text.toString().trim())
        })

        viewModel.errorRegister.observe(this, {
            waitingDialog!!.dismiss()
            when (it) {
                "phone" -> binding.tiPhone.error =
                    "Este teléfono ya está registrado, por favor ingrese uno nuevo o inicie sesión."
                "username" -> {
                    errorUserName =
                        "Este usuario ya está registrado, por favor ingrese uno nuevo o inicie sesión."
                    onBack()
                }
                "document_number" -> {
                    errorDocument =
                        "Este documento ya está registrado, por favor ingrese uno nuevo o inicie sesión."
                    onBack()
                }
                "email" -> binding.tiEmail.error =
                    "Este email ya está registrado, por favor ingrese uno nuevo o inicie sesión."
                else -> {
                    Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun onBack() {
        val intent = Intent(this, RegisterOneActivity::class.java).apply {
            putExtra("typeDocument", typeDocument)
            putExtra("document", document)
            putExtra("names", names)
            putExtra("lastNames", lastNames)
            putExtra("errorUserName", errorUserName)
            putExtra("email", binding.etEmail.text.toString().trim())
            putExtra("city", binding.filledCity.text.toString().trim())
            putExtra("idCity", idCity)
            putExtra("direction", binding.etDirection.text.toString().trim())
            putExtra("phone", binding.etPhone.text.toString().trim())
            putExtra("password", binding.etPassword.text.toString().trim())
            putExtra("errorDocument", errorDocument)
            putExtra("userName", userName)
//            putExtra("errorEmail", errorEmail)
        }
        startActivity(intent)
        finish()
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun showLoading(message: String) {
        val inflater = layoutInflater
        val view =
            inflater.inflate(R.layout.item_view_loading, findViewById(android.R.id.content), false)
        val binding: ItemViewLoadingBinding = ItemViewLoadingBinding.bind(view)
        binding.txtMessage.text = message
        val alerta = MaterialAlertDialogBuilder(this)
        alerta.setView(view)
        alerta.setCancelable(true)
//        alerta.setNegativeButton("Cancelar") { dialog, which ->
//
//        }
        waitingDialog = alerta.create()
        waitingDialog!!.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        onBack()
    }
}