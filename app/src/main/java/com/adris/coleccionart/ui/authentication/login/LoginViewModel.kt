package com.adris.coleccionart.ui.authentication.login

import android.text.BoringLayout
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adris.coleccionart.common.dataApi.DataDocument
import com.adris.coleccionart.common.entities.CitiesVO
import com.adris.coleccionart.ui.authentication.RepositoryAuthentication
import com.adris.coleccionart.ui.authentication.ResponseAuthentication
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel(), ResponseAuthentication {

    private val repository = RepositoryAuthentication.instance(this)

    val login: LiveData<Boolean> get() = _login
    val updatePassword: LiveData<Boolean> get() = _updatePassword
    val forgorPassword: LiveData<JsonObject> get() = _forgorPassword
    val errorLogin: LiveData<String> get() = _errorLogin
    val errorForgot: LiveData<String> get() = _errorForgot
    val errorUpdatePassword: LiveData<String> get() = _errorUpdatePassword

    private var _login = MutableLiveData<Boolean>()
    private var _updatePassword = MutableLiveData<Boolean>()
    private var _forgorPassword = MutableLiveData<JsonObject>()
    private var _errorLogin = MutableLiveData<String>()
    private var _errorForgot = MutableLiveData<String>()
    private var _errorUpdatePassword = MutableLiveData<String>()

    fun loginUser(emailOrUserName: String, password: String) {
        viewModelScope.launch {
            repository.login(emailOrUserName, password)
        }
    }

    fun forgotPassword(emailOrUserName: String) {
        viewModelScope.launch {
            repository.forgotPassword(emailOrUserName)
        }
    }

    fun updatePasswordUser(
        email: String,
        token: String,
        password: String,
        password_confirmation: String
    ) {
        viewModelScope.launch {
            repository.updatePassword(
                email,
                token,
                password,
                password_confirmation
            )
        }
    }

    override fun successVerifyDocument(data: DataDocument) {

    }

    override fun cities(cities: ArrayList<CitiesVO>) {

    }

    override fun citiesEmpty(error: String) {

    }

    override fun errorCities(error: String) {

    }

    override fun errorVerifyDocument(error: String) {

    }

    override fun successRegister(success: Boolean) {

    }

    override fun okCode(success: Boolean) {

    }

    override fun newCode(success: Boolean) {

    }

    override fun errorRegister(error: String) {

    }

    override fun errorCode(error: String) {

    }

    override fun errorNewCode(error: String) {

    }

    override fun login(success: Boolean) {
        _login.postValue(success)
    }

    override fun errorLogin(error: String) {
        _errorLogin.postValue(error)
    }

    override fun forgotPassword(data: JsonObject) {
        _forgorPassword.postValue(data)
    }

    override fun errorForgotPassword(error: String) {
        _errorForgot.postValue(error)
    }

    override fun resetPassword(success: Boolean) {
        _updatePassword.postValue(success)
    }

    override fun errorResetPassword(error: String) {
        _errorUpdatePassword.postValue(error)
    }
}