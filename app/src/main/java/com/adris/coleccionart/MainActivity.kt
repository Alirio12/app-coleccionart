package com.adris.coleccionart

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.TextView
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.adris.coleccionart.common.preference.PreferenceModule
import com.adris.coleccionart.common.singleton.InstanceRetrofit
import com.adris.coleccionart.databinding.ActivityMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var headerView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        PreferenceModule.getInstance(this)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
//        val nav: NavigationView = findViewById(R.id.nav_view)


//        nameUser = findViewById(R.id.txt_name)
//        emailUser = findViewById(R.id.txt_email)
        setSupportActionBar(toolbar)

        Log.d("errorRe", InstanceRetrofit.listTypeDocuments.toString())

//        val fab: FloatingActionButton = findViewById(R.id.fab)
//        fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
//        }


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        headerView = navView.getHeaderView(0)

        val navController = findNavController(R.id.nav_host_fragment)
        val bottom: BottomNavigationView = findViewById(R.id.bottomNavigationView)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_notification,
                R.id.nav_subasta,
                R.id.nav_profile,
                R.id.nav_not_login
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        bottom.setupWithNavController(navController)



        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            if (destination.id == R.id.nav_profile) {

//                val intent = Intent(this, VerifyCodeActivity::class.java)
//                startActivity(intent)
//                controller.navigate(R.id.nav_profile)
//                bottom.isVisible = false
            }
        }


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onStart() {
        super.onStart()
        if (PreferenceModule.getInstance().existData() && PreferenceModule.getInstance()
                .getVerified()
        ) {
            val nameUser: TextView = headerView.findViewById(R.id.txt_name)
            val emailUser: TextView = headerView.findViewById(R.id.txt_email)

            nameUser.text = PreferenceModule.getInstance().getName()
            emailUser.text = PreferenceModule.getInstance().getEmail()
        }
    }
}