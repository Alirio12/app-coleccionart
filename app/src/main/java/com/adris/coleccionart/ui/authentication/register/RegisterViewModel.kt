package com.adris.coleccionart.ui.authentication.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adris.coleccionart.common.dataApi.DataDocument
import com.adris.coleccionart.common.entities.CitiesVO
import com.adris.coleccionart.ui.authentication.RepositoryAuthentication
import com.adris.coleccionart.ui.authentication.ResponseAuthentication
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class RegisterViewModel : ViewModel(), ResponseAuthentication {

    private val repository = RepositoryAuthentication.instance(this)

    val dataDocument: LiveData<DataDocument> get() = _dataDocument
    val cities: LiveData<ArrayList<CitiesVO>> get() = _cities
    val register: LiveData<Boolean> get() = _register
    val code: LiveData<Boolean> get() = _code
    val newCode: LiveData<Boolean> get() = _newCode

    val errorDocument: LiveData<String> get() = _errorDocument
    val errorCities: LiveData<String> get() = _errorCities
    val citiesEmpty: LiveData<String> get() = _citiesEmpty
    val errorRegister: LiveData<String> get() = _errorRegister
    val errorCode: LiveData<String> get() = _errorCode
    val errorNewCode: LiveData<String> get() = _errorNewCode


    private var _dataDocument = MutableLiveData<DataDocument>()
    private var _cities = MutableLiveData<ArrayList<CitiesVO>>()
    private var _register = MutableLiveData<Boolean>()
    private var _code = MutableLiveData<Boolean>()
    private var _newCode = MutableLiveData<Boolean>()

    private var _errorDocument = MutableLiveData<String>()
    private var _errorCities = MutableLiveData<String>()
    private var _citiesEmpty = MutableLiveData<String>()
    private var _errorRegister = MutableLiveData<String>()
    private var _errorCode = MutableLiveData<String>()
    private var _errorNewCode = MutableLiveData<String>()

    fun verifyDocument(type: String, document: String) {
        viewModelScope.launch {
            repository.verifyDocument(type, document)
        }
    }

    fun loadCities(city: String) {
        viewModelScope.launch {
            repository.loadCities(city)
        }
    }

    fun registerUser(
        first_name: String,
        last_name: String,
        email: String,
        username: String,
        document_type: String,
        document_number: String,
        address: String,
        phone: String,
        city_id: Long,
        password: String,
        device_token: String,
    ) {
        viewModelScope.launch {
            repository.registerUser(
                first_name,
                last_name,
                email,
                username,
                document_type,
                document_number,
                address,
                phone,
                city_id,
                password,
                device_token,
            )
        }
    }

    fun verifyCode(email: String, code: String) {
        viewModelScope.launch {
            repository.verifyCode(email, code)
        }
    }

    fun sendNewCode(email: String) {
        viewModelScope.launch {
            repository.sendNewCode(email)
        }
    }

    override fun successVerifyDocument(data: DataDocument) {
        _dataDocument.postValue(data)
    }

    override fun cities(cities: ArrayList<CitiesVO>) {
        _cities.postValue(cities)
    }

    override fun citiesEmpty(error: String) {
        _citiesEmpty.postValue(error)
    }

    override fun errorCities(error: String) {
        _errorCities.postValue(error)
    }

    override fun errorVerifyDocument(error: String) {
        _errorDocument.postValue(error)
    }

    override fun successRegister(success: Boolean) {
        _register.postValue(success)
    }

    override fun okCode(success: Boolean) {
        _code.postValue(success)
    }

    override fun newCode(success: Boolean) {
        _newCode.postValue(success)
    }

    override fun errorRegister(error: String) {
        _errorRegister.postValue(error)
    }

    override fun errorCode(error: String) {
        _errorCode.postValue(error)
    }

    override fun errorNewCode(error: String) {
        _errorNewCode.postValue(error)
    }

    override fun login(success: Boolean) {

    }

    override fun errorLogin(error: String) {

    }

    override fun forgotPassword(data: JsonObject) {

    }

    override fun errorForgotPassword(error: String) {

    }

    override fun resetPassword(success: Boolean) {

    }

    override fun errorResetPassword(error: String) {

    }
}